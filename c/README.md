This is the C implementation of the [simple-data spec][sda]

# Compiling

cc simple-data.c `pkgconf --libs --cflags libzstd` -lzstd

# Example

A word of caution as `sda_put` use variadic arguments, care should be
taken, as the compiler can't perform argument checking, that:
* the correct number of arguments are given
  (the same as the number of fields defined in the specifications)
* argument are of the correct type
  (if using literal instead of variable, as all the following are not the
  same: `1`, `1l`, `1ll`, `1ul`, `1.0f`, `1.0`,  ...)

~~~c
sda_definition_t sda_def = {
    // Tags (optional)
    .tags   = (struct sda_tag   []) {
        SDA_TAG(AUTHOR,  "Stephane D'Alu"       ),
        SDA_TAG(URL,     "http://www.sdalu.com/"),
        SDA_TAG(LICENSE, "MIT"                  ),
        SDA_TAG_END,
    },
	// Fields description (required)
    .fields = (struct sda_field []) {
        SDA_FIELD(CSTR, "name"         "spacecraft"),
        SDA_FIELD(CSTR, "origin"       "serie or movie"),
        SDA_FIELD(F64,  "x",           NULL),
        SDA_FIELD(F64,  "y",           NULL),
        SDA_FIELD(F64,  "z",           NULL),
        SDA_FIELD_END,
    }
};

sda_t sda = sda_open("spacecraft.sda", SDA_O_CREATE, &sda_def);
sda_data_compress(sda, 1);

sda_put(sda, "Enterprise", "Star Trek",    1.0, 2.0, 3.0);
sda_put(sda, "Rocinante",  "The Expanse",  1e6, 2e6, 3e6);
sda_put(sda, "Serenity",   "Firefly",      0.0, 0.0, 0.0);
sda_put(sda, "Bebop",      "Cowboy Bebop", 4.0, 6.8, 8.0);

sda_close(sda);
~~~

[sda]: https://gitlab.inria.fr/dalu/simple-data/
