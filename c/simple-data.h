#ifndef SIMPLE_DATA_H
#define SIMPLE_DATA_H

#include <inttypes.h>
#include <stdio.h>

/**
 * Last version of the file format handled
 */
#define SDA_VERSION 1


/**
 * Blob structure. Capacity max is 65535.
 */
struct sda_blob {
    uint16_t size;
    char    *data;
};


/*
 * Tag definitions
 */

/**
 * Tag definition.
 */
struct sda_tag {
    char *name;
    char *value;
};

/**
 * Initialise tag.
 *
 * Possible values for tag names are:
 *   TITLE, SUMMARY, AUTHOR, LICENSE, URL, DOI, KEYWORDS
 */
#define SDA_TAG(tname, str)			\
    SDA_TAG_##tname(str)

/**
 * Use to mark end of tag list.
 */
#define SDA_TAG_END				\
    { .name = NULL }

#define _SDA_TAG(_name, _str)			\
    { .name = _name,    .value = _str }    

#define SDA_TAG_TITLE(str)			\
    _SDA_TAG("title", str)
#define SDA_TAG_SUMMARY(str)			\
    _SDA_TAG("sumary", str)
#define SDA_TAG_AUTHOR(str)			\
    _SDA_TAG("author", str)
#define SDA_TAG_LICENSE(str)			\
    _SDA_TAG("license", str)
#define SDA_TAG_URL(str)			\
    _SDA_TAG("url", str)
#define SDA_TAG_DOI(str)			\
    _SDA_TAG("doi", str)
#define SDA_TAG_KEYWORDS(str)			\
    _SDA_TAG("keywords", str)



/*
 * Field definitions
 */

/**
 * Field definition.
 */
struct sda_field {
    uint32_t type;
    char    *name;
    char    *unit;
    char    *description;
};

/**
 * Initialise field.
 *
 * Possible values for tag names are:
 *   I8, I16, I32, I64, U8, U16, U32, U64, F32, F64, CSTR, BLOB, CHAR, BOOL
 */
#define SDA_FIELD(fname, name, unit, desc)	\
    SDA_FIELD_##fname(name, unit, desc)

/**
 * Use to mark end of field list.
 */
#define SDA_FIELD_END				\
    _SDA_FIELD(0, NULL, NULL, NULL)


#if __BYTE_ORDER == __LITTLE_ENDIAN
#define SDA_I8_TYPE			0x20203869  // i8
#define SDA_I16_TYPE			0x20363169  // i16
#define SDA_I32_TYPE			0x20323369  // i32
#define SDA_I64_TYPE			0x20343669  // i64
#define SDA_U8_TYPE			0x20203875  // u8
#define SDA_U16_TYPE			0x20363175  // u16
#define SDA_U32_TYPE			0x20323375  // u32
#define SDA_U64_TYPE			0x20343675  // u64
#define SDA_F32_TYPE			0x20323366  // f32
#define SDA_F64_TYPE			0x20343666  // f64
#define SDA_CSTR_TYPE			0x72747363  // cstr
#define SDA_BLOB_TYPE			0x626f6c62  // blob
#define SDA_CHAR_TYPE			0x72616863  // char
#define SDA_BOOL_TYPE			0x6c6f6f62  // bool
#elif __BYTE_ORDER == __BIG_ENDIAN
#define SDA_I8_TYPE			0x69382020  // i8
#define SDA_I16_TYPE			0x69313620  // i16
#define SDA_I32_TYPE			0x69333220  // i32
#define SDA_I64_TYPE			0x69363420  // i64
#define SDA_U8_TYPE			0x75382020  // u8
#define SDA_U16_TYPE			0x75313620  // u16
#define SDA_U32_TYPE			0x75333220  // u32
#define SDA_U64_TYPE			0x75363420  // u64
#define SDA_F32_TYPE			0x66333220  // f32
#define SDA_F64_TYPE			0x66363420  // f64
#define SDA_CSTR_TYPE			0x63737472  // cstr
#define SDA_BLOB_TYPE			0x626c6f62  // blob
#define SDA_CHAR_TYPE			0x63686172  // char
#define SDA_BOOL_TYPE			0x626f6f6c  // bool
#else
#error "unhandled byte order"
#endif

#define _SDA_FIELD(_type, _name, _unit, _desc)			\
    { .type = _type, .name = _name, .unit = _unit, .description = _desc }


#define SDA_FIELD_I8(name, unit, desc)			\
    _SDA_FIELD(SDA_I8_TYPE, name, unit, desc)
#define SDA_FIELD_I16(name, unit, desc)			\
    _SDA_FIELD(SDA_I16_TYPE, name, unit, desc)
#define SDA_FIELD_I32(name, unit, desc)			\
    _SDA_FIELD(SDA_I32_TYPE, name, unit, desc)
#define SDA_FIELD_I64(name, unit, desc)			\
    _SDA_FIELD(SDA_I64_TYPE, name, unit, desc)
#define SDA_FIELD_U8(name, unit, desc)			\
    _SDA_FIELD(SDA_U8_TYPE, name, unit, desc)
#define SDA_FIELD_U16(name, unit, desc)			\
    _SDA_FIELD(SDA_U16_TYPE, name, unit, desc)
#define SDA_FIELD_U32(name, unit, desc)			\
    _SDA_FIELD(SDA_U32_TYPE, name, unit, desc)
#define SDA_FIELD_U64(name, unit, desc)			\
    _SDA_FIELD(SDA_U64_TYPE, name, unit, desc)
#define SDA_FIELD_F32(name, unit, desc)			\
    _SDA_FIELD(SDA_F32_TYPE, name, unit, desc)
#define SDA_FIELD_F64(name, unit, desc)			\
    _SDA_FIELD(SDA_F64_TYPE, name, unit, desc)
#define SDA_FIELD_CSTR(name, unit, desc)		\
    _SDA_FIELD(SDA_CSTR_TYPE, name, unit, desc)
#define SDA_FIELD_BLOB(name, unit, desc)		\
    _SDA_FIELD(SDA_BLOB_TYPE, name, unit, desc)
#define SDA_FIELD_CHAR(name, unit, desc)		\
    _SDA_FIELD(SDA_CHAR_TYPE, name, unit, desc)
#define SDA_FIELD_BOOL(name, unit, desc)		\
    _SDA_FIELD(SDA_BOOL_TYPE, name, unit, desc)


/**
 * Simple data definition.
 */
typedef struct sda_definition {
    struct sda_field *fields;
    struct sda_tag   *tags;
} sda_definition_t;


/**
 * Simple data handler (opaque pointer)
 */
typedef struct sda *sda_t;

/**
 * Opening mode
 */
#define SDA_O_CREATE		0x00  /**< Create     */
#define SDA_O_READONLY		0x01  /**< Read only  */
#define SDA_O_APPEND		0x02  /**< Append     */

/**
 * Open a file for reading or creation.
 */
sda_t sda_open(char *file, int mode, sda_definition_t *def);

/**
 * Enable data compression.
 *
 * @note Compression can only by enabled *before* putting data
 */
int sda_data_compress(sda_t sda, int enable);

/**
 * Write a whole new section.
 *
 * All the line will be automatically prefixed with '# '
 */
int sda_write_section(sda_t sda, char *name, char *text);

/**
 * Write a single new line.
 *
 * Line will we be automatically prefixed with '# ' and return carriage added
 *
 * @note Don't manually use line return!
 */
int sda_printfln(sda_t sda, const char *fmt, ...)
    __attribute__ ((format (printf, 2, 3)));

/**
 * Put a new data
 *
 * @note This is a variadic function. It means that the compiler is not
 *       able to check arguments being passed (types or count).
 *       Care should be taken to strictly respect the types and count
 *       defined in the fields definition.
 *       It also means that literal values should have the exact requested
 *       type, use the type modifier if necessary: 0l, 0ll, 0ull, 1.0, 1.0f
 */
int sda_put(sda_t sda, ...);

/**
 * Get a new data
 *
 * @note This is a variadic function. It means that the compiler is not
 *       able to check arguments being passed (types or count).
 */
int sda_get(sda_t sda, ...);

/**
 * Close file.
 *
 * @note File should always be closed to ensure all data are flushed
 */
void sda_close(sda_t sda);

#endif
