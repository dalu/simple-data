#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>

#if SIMPLEDATA_WITH_COMPRESSION
#include <zstd.h>
#endif

#include <simple-data.h>

#define SDA_ROUNDUP(x, y)   ((((x) + (y) - 1) / (y)) * (y))

/* Internal state
 */
#define SDA_STATE_EMPTY      0  // At allocation time
#define SDA_STATE_META       1  // Meta data section 
#define SDA_STATE_DATAREAD   2  // Data section read-mode
#define SDA_STATE_DATAWRITE  3  // Data section write-mode

/* Buffer
 */
#define SDA_BUFFER_UNITSIZE   (32 * 1024)    // Minimum allocation unit
#define SDA_BUFFER_MINFREE    128            // Ensure min space for long write

/* Context handler
 */
struct sda {
    sda_definition_t *def;   // Definition
    int fd;                  // File description
    int mode;                // File opening mode
    int state;               // Internal state
    int compress;            // Compression request
    char  *buf;              // Buffer
    size_t buflen;           // Buffer length
    char  *bufptr;           // Pointer to buffer write position
    size_t bufdatalen;       // Buffer actual data length

#if SIMPLEDATA_WITH_COMPRESSION    
    struct {
	int    enabled;      // Is comression currently enabled
	char  *buf;          // Compression buffer
	size_t buflen;       // Compression buffer length
	union {              // Compressor internal context
	    struct {
		ZSTD_CCtx* cctx;
		ZSTD_DCtx* dctx;
		ZSTD_inBuffer input;
	    } zstd;
	};
    } compression;
#endif
};


/*
 * Forward declaration
 */
static int sda_printf(sda_t sda, const char *fmt, ...)
    __attribute__ ((format (printf, 2, 3)));

static ssize_t sda_write(sda_t sda, void *buf, size_t buflen);
static int sda_decompress_init(sda_t sda);



/*
 * Low level I/O operations
 */
static ssize_t
sda_io_write(sda_t sda, void *buf, size_t buflen)
{
#if SIMPLEDATA_WITH_COMPRESSION
    // https://zstd.docsforge.com/dev/code-examples/streaming_compression.c/
    if (sda->compression.enabled) {
	int size = 0;
	ZSTD_inBuffer input = { sda->buf, sda->bufptr - sda->buf, 0 };
	do {
	    ZSTD_outBuffer output = {
		sda->compression.buf, sda->compression.buflen, 0 };
	    ZSTD_compressStream2(sda->compression.zstd.cctx,
				 &output , &input, ZSTD_e_continue);
	    int rc = write(sda->fd, output.dst, output.pos);
	    if (rc < 0) return rc;
	    size += rc;
	} while (input.pos != input.size);
	return size;
    }
#endif
    
    return write(sda->fd, buf, buflen);
}

static ssize_t
sda_io_read(sda_t sda, void *buf, size_t buflen)
{
#if SIMPLEDATA_WITH_COMPRESSION
    // https://zstd.docsforge.com/dev/code-examples/streaming_compression.c/
    if (sda->compression.enabled) {
    redo:
	if (sda->compression.zstd.input.pos ==
	    sda->compression.zstd.input.size) {
	    ssize_t rc = read(sda->fd,
			      sda->compression.buf, sda->compression.buflen);
	    if (rc <= 0) {
		return rc;
	    }
	    sda->compression.zstd.input = (ZSTD_inBuffer) {
		sda->compression.buf, rc, 0
	    };
	}
	ZSTD_outBuffer output = { buf, buflen, 0 };
	size_t ret = ZSTD_decompressStream(sda->compression.zstd.dctx, &output , &sda->compression.zstd.input);
	if (output.pos == 0) {
	    goto redo;
	}
	return output.pos;
    }
#endif
	
    return read(sda->fd, buf, buflen);
}

/*
 * Buffer operations
 */
    static ssize_t
sda_buffer_flush(sda_t sda)
{
    ssize_t rc = sda_io_write(sda, sda->buf, sda->bufptr - sda->buf);
    sda->bufptr = sda->buf;
    return rc;
}

static int
sda_buffer_resize(sda_t sda, int size)
{
    // Resize should only be used on empty buffer
    assert(sda->buf == sda->bufptr);

    // Roundup size
    size = SDA_ROUNDUP(size, SDA_BUFFER_UNITSIZE);

    // Reallocate
    char *buf = realloc(sda->buf, size);
    if (buf == NULL) return -1;
    sda->buflen               = size;
    sda->buf    = sda->bufptr = buf;

    // ok
    return size;
}


/*
 * Print operation
 */
static int
sda_vprintf(sda_t sda, const char *fmt, va_list args)
{
    int rc    = 0; // return code
    int ssize = 0; // printf string size
	
    // Create a copy of args, as we can need to call again
    va_list args_retry;
    va_copy(args_retry, args);
    
    // Buffer remaining space
    size_t buffree = sda->buf + sda->buflen - sda->bufptr;
    
    // Ensure we have enough space for an average write
    if (buffree < SDA_BUFFER_MINFREE) {
	rc = sda_buffer_flush(sda);
	if (rc < 0) goto failed;
	buffree = sda->buflen;
    }

    // Write data
    rc = ssize = vsnprintf(sda->bufptr, buffree, fmt, args);
    if (rc < 0) goto failed;
    
    // If not enough space, retry
    if (ssize >= buffree) {
	// Flush buffer
	rc = sda_buffer_flush(sda);
	if (rc < 0) goto failed;
	
	// Buffer is too small anyway, resize it
	if (ssize > sda->buflen) {
	    rc = sda_buffer_resize(sda, ssize);
	    if (rc < 0) goto failed;
	}
	
	// Retry with an empty buffer
	rc = ssize = vsnprintf(sda->bufptr, sda->buflen, fmt, args_retry);
	if (rc < 0) return rc;	    
    }
    sda->bufptr += ssize;
    
 failed:
    va_end(args_retry);
    return rc;
}


static int
sda_printf(sda_t sda, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    int rc = sda_vprintf(sda, fmt, args);
    va_end(args);
    return rc;
}

int
sda_printfln(sda_t sda, const char *fmt, ...)
{
    int rc;
    int size = 0;
    va_list args;

    rc = sda_write(sda, "# ", 2);
    if (rc < 0) return rc;
    size += rc;
    
    va_start(args, fmt);
    rc = sda_vprintf(sda, fmt, args);
    va_end(args);
    if (rc < 0) return rc;
    size += rc;
    
    rc = sda_write(sda, "\n", 1);
    if (rc < 0) return rc;
    size += rc;
    
    return size;
}

/*
 * Write operations
 */
static ssize_t
sda_write(sda_t sda, void *buf, size_t buflen)
{
    int rc;
    
    // Buffer remaining space
    size_t buffree = sda->buf + sda->buflen - sda->bufptr;

    // If not enough space
    if (buflen > buffree) {
	// Flush buffer
	rc = sda_buffer_flush(sda);
	if (rc < 0) return rc;

	// If we will fill half the buffer (or buffer too short)
	// use in place write
	if (buflen > sda->buflen / 2) {
	    rc = sda_io_write(sda, buf, buflen);
	    return rc;
	}
    }

    // Copy
    memcpy(sda->bufptr, buf, buflen);
    sda->bufptr += buflen;

    return buflen;
}

static ssize_t
sda_getline(sda_t sda, char **line)
{
    char   *str = NULL;
    size_t  off = 0;
    char *  nl  = NULL;
    size_t size = 0;

    
    do {
	// If buffer empty, fill it.
	if (sda->bufdatalen == 0) {
	    ssize_t rc      = sda_io_read(sda, sda->buf, sda->buflen);
	    if      (rc <  0) { free(*line) ; return rc; }  // Error
	    else if (rc == 0) { free(*line) ; return 0;  }  // End of file
	    sda->bufptr     = sda->buf;
	    sda->bufdatalen = rc;	
	} 
	
	nl   = memchr(sda->bufptr, '\n', sda->bufdatalen);
	size = sda->bufdatalen;

	if (nl != NULL) {
	    size = nl - sda->bufptr + 1;
	}
	
	char  *mem  = realloc(str, off + size);
	if (mem == NULL) {
	    free(str);
	    return -1;
	}
	str = mem;
	
	memcpy(&str[off], sda->bufptr, size);
	sda->bufptr     += size;
	sda->bufdatalen -= size;
	off             += size;

    } while (nl == NULL);

    if ((off > 1) && (str[off - 1] == '\n')) {
	str[off-1] = '\0';
	if ((off > 2) && (str[off - 2] == '\r'))
	    str[off-2] = '\0';
    }
    str[off] = '\0';
    *line = str;
    
    return off;
}

static ssize_t
sda_read(sda_t sda, void *buf, size_t buflen)
{
    ssize_t len = 0;
    do {
	// If buffer empty, re-fill
	if (sda->bufdatalen == 0) {
	    ssize_t rc      = sda_io_read(sda, sda->buf, sda->buflen);
	    if      (rc <  0) { return rc;       }  // Error
	    else if (rc == 0) { return len + rc; }  // End of file
	    sda->bufptr     = sda->buf;
	    sda->bufdatalen = rc;
	}

	// Read data
	size_t readlen = buflen > sda->bufdatalen ? sda->bufdatalen : buflen;
	memcpy(buf, sda->bufptr, readlen);
	len             += readlen;
	buflen          -= readlen;
	sda->bufdatalen -= readlen;
	sda->bufptr      = ((char *)sda->bufptr) + readlen;
    } while(buflen > 0);

    return len;
}

static int
sda_write_magic(sda_t sda) {
    return sda_printf(sda, "# simple-data:%d\n", 1);
}

static int
sda_write_section_title(sda_t sda, char *name) {
    return sda_printf(sda, "#\n# --<%s>--\n", name);
}

static int
sda_write_meta(sda_t sda)
{
    if (sda->def->tags == NULL)
	return 0;

    int rc;
    int size   = 0;
    int taglen = 8;

    rc = sda_write(sda, "#\n", 2);
    if (rc < 0) return rc;
    size += rc;
    
    for (struct sda_tag *tag = sda->def->tags ; tag->name ; tag++) {
	rc = sda_printf(sda, "# @%-*s %s\n", taglen, tag->name, tag->value);
	if (rc < 0) return rc;
	size += rc;
    }

    return size;
}

static int
sda_write_spec(sda_t sda) {
    int rc;
    int size = 0;

    rc = sda_write_section_title(sda, "spec");
    if (rc < 0) return rc;
    size += rc;

    int name_maxlen = 0;
    int unit_maxlen = 0;
    for (struct sda_field *field = sda->def->fields ; field->type ; field++) {
	int name_len = strlen(field->name);
	if (name_len > name_maxlen) name_maxlen = name_len;
	if (field->unit) {
	    int unit_len = strlen(field->unit);
	    if (unit_len > unit_maxlen) unit_maxlen = unit_len;
	}
    }
    
    for (struct sda_field *field = sda->def->fields ; field->type ; field++) {
	// Note: field->type is 4 bytes, so it is ok to use %4s
	if (field->description) {
	    if (field->unit) {
		rc = sda_printf(sda, "# %4s : %-*s [%-*s] (%s)\n", (char *)&field->type,
				name_maxlen, field->name,
				unit_maxlen, field->unit,
				field->description);
	    } else {
		rc = sda_printf(sda, "# %4s : %-*s  %-*s  (%s)\n", (char *)&field->type,
				name_maxlen, field->name,
				unit_maxlen, "",
				field->description);
	    }
	} else {
	    if (field->unit) {
		rc = sda_printf(sda, "# %4s : %-*s [%-*s]\n", (char *)&field->type,
				name_maxlen, field->name,
				unit_maxlen, field->unit);
	    } else {
		rc = sda_printf(sda, "# %4s : %s\n", (char *)&field->type, field->name);
	    }
	}
	if (rc < 0) return rc;
	size += rc;
    }

    return size;
}

int
sda_write_section(sda_t sda, char *name, char *text)
{
    int rc;
    int size = 0;
    
    rc = sda_write_section_title(sda, name);
    if (rc < 0) return rc;
    size += rc;
    
    if (text == NULL || *text == '\0')
	return size;
    
    do {
	char *nline = strchrnul(text, '\n');
	char *eol   = *nline == '\0' ? nline : nline + 1;
	rc = sda_write(sda, "# ", 2);
	if (rc < 0) return rc;
	size += rc;
	
	rc = sda_write(sda, text, eol - text);
	if (rc < 0) return rc;
	size += rc;
	
	if (*nline == '\0') {
	    rc = sda_write(sda, "\n", 1);
	    if (rc < 0) return rc;
	    size += rc;
	}
	text = eol;
    } while(*text != '\0');

    return size;
}

static int
sda_write_textual(sda_t sda)
{
    if ((sda_write_magic(sda) < 0) ||
	(sda_write_meta(sda)  < 0) ||
	(sda_write_spec(sda)  < 0))
	return -1;
    
    sda->state = SDA_STATE_META;
    return 0;
}


/*
 * Read operation
 */
static int
sda_read_magic(sda_t sda, int *version)
{
    char *line;
    if (sda_getline(sda, &line) < 0) {
	free(line);
	return -1;
    }
    free(line);
    return sscanf(line, "# simple-data:%d", version);
}


static int
sda_read_meta(sda_t sda)
{
    char *line;
    int pos;
    if (sda_getline(sda, &line) < 0) {
	free(line);
	return -1;
    }

    while ((sscanf(line, "# \n%n", &pos) >= 0) && (pos == strlen(line))) {
	if (sda_getline(sda, &line) < 0) {
	    free(line);
	    return -1;
	}
    }

    return 0;
}

static int
sda_read_textual(sda_t sda)
{
    int rc;
    char *line;
    int version;
    
    if ((rc = sda_read_magic(sda, &version)) < 0)
	return rc;
    
    for (;;) {
	if (sda_getline(sda, &line) < 0) {
	    free(line);
	    return -1;
	}
	
	if        (!strcmp("# --<data>--", line)) {
	    return 0;
	} else if (!strcmp("# --<data:compressed>--", line)) {
	    sda_decompress_init(sda);
	    return 0;
	}
    }
}


/* Intialise compression
 */

static int
sda_decompress_init(sda_t sda)
{
#if SIMPLEDATA_WITH_COMPRESSION
    memset(&sda->compression, 0, sizeof(sda->compression));
    
    // Allocate compression buffer
    sda->compression.buflen = ZSTD_DStreamInSize();
    sda->compression.buf    = malloc(sda->compression.buflen);
    if (sda->compression.buf == NULL) goto failed;

    // Migrate remaining buffer data to compression buffer
    if (sda->bufdatalen > 0) {
	assert(sda->compression.buflen >= sda->buflen);
	memcpy(sda->compression.buf, sda->bufptr, sda->bufdatalen);
	sda->compression.zstd.input = (ZSTD_inBuffer) {
	    sda->compression.buf, sda->bufdatalen, 0
	};
	sda->bufptr     = sda->buf;
	sda->bufdatalen = 0;
    }
    // Allocate buffer according to compression preferences (if greater)
    size_t buflen  = ZSTD_DStreamOutSize();
    if (buflen > sda->buflen) {
	char  *buf     = realloc(sda->buf, buflen);
	if (buf == NULL) goto failed;
	sda->buflen             = buflen;
	sda->buf = sda->bufptr  = buf;
    }

    // Create context
    ZSTD_DCtx *dctx = sda->compression.zstd.dctx = ZSTD_createDCtx();
    if (dctx == NULL) goto failed;

    // Done
    sda->compression.enabled = 1;
    return 0;
    
 failed:
    free(sda->compression.buf);
    ZSTD_freeDCtx(dctx);
    return -1;
#else
    return -1;
#endif
}

static int
sda_compress_init(sda_t sda)
{
#if SIMPLEDATA_WITH_COMPRESSION
    memset(&sda->compression, 0, sizeof(sda->compression));
    
    // Allocate buffer according to compression preferences (if greater)
    size_t buflen  = ZSTD_CStreamInSize();
    if (buflen > sda->buflen) {
	char  *buf     = realloc(sda->buf, buflen);
	if (buf == NULL) goto failed;
	sda->buflen             = buflen;
	sda->buf = sda->bufptr  = buf;
    }

    // Allocate compression buffer
    sda->compression.buflen = ZSTD_CStreamOutSize();
    sda->compression.buf    = malloc(sda->compression.buflen);
    if (sda->compression.buf == NULL) goto failed;

    // Create context
    ZSTD_CCtx *cctx = sda->compression.zstd.cctx = ZSTD_createCCtx();
    if (cctx == NULL) goto failed;

    // Apply preferences
    ZSTD_CCtx_setParameter(cctx, ZSTD_c_compressionLevel, 9);
    ZSTD_CCtx_setParameter(cctx, ZSTD_c_checksumFlag,     1);
  //ZSTD_CCtx_setParameter(cctx, ZSTD_c_nbWorkers,        2);

    // Done
    sda->compression.enabled = 1;
    return 0;

 failed:
    free(sda->compression.buf);
    ZSTD_freeCCtx(cctx);
    return -1;
#else
    return -1;
#endif
}


/*
 */

sda_t
sda_open(char *file, int mode, sda_definition_t *def)
{
    // File descriptor mode
    int fd_mode;
    switch(mode) {
    case SDA_O_CREATE:    fd_mode = O_CREAT | O_WRONLY | O_TRUNC;  break;
    case SDA_O_READONLY:  fd_mode = O_RDONLY;                      break;
    case SDA_O_APPEND:    fd_mode = O_RDWR;                        break;
    default: return NULL;
    }

    // Allocate context
    sda_t sda = calloc(sizeof(struct sda), 1);
    if (sda == NULL) goto failed;

    // Definition
    sda->def  = def;
    sda->mode = mode;
	
    // File descriptor
    sda->fd   = open(file, fd_mode, 0644);
    if (sda->fd < 0) goto failed;
    
    // Allocate buffer
    sda->buflen = SDA_BUFFER_UNITSIZE;
    sda->buf = sda->bufptr = malloc(sda->buflen);
    if (sda->buf == NULL) goto failed;
	
    // Textual part
    if (mode == SDA_O_CREATE) {
	if (sda_write_textual(sda) < 0)
	    goto failed;
    } else {
	if (sda_read_textual(sda) < 0)
	    goto failed;
	sda->state = SDA_STATE_DATAREAD;
    }

    // Done
    return sda;

    // Failed
 failed:
#if SIMPLEDATA_WITH_COMPRESSION    
    if (sda && sda->compression.buf) free(sda->compression.buf);
#endif
    if (sda && sda->buf)             free(sda->buf);
    if (sda && sda->fd >= 0)         close(sda->fd);
    if (sda)                         free(sda);
    return NULL;
}


int
sda_data_compress(sda_t sda, int enable)
{
#if SIMPLEDATA_WITH_COMPRESSION    
    // Can only be selected at creation time
    if (sda->mode != SDA_O_CREATE)
	return 0;

    // Not possible once in the data section
    if (sda->state >= SDA_STATE_DATAREAD)
	return -1;

    // Enable compression
    sda->compress = enable;

    // Done
    return 0;
#else
    return -1;
#endif
}


void
sda_close(sda_t sda) {
    if (sda != NULL) {
	if (sda->mode != SDA_O_READONLY)
	    sda_buffer_flush(sda);

#if SIMPLEDATA_WITH_COMPRESSION
	if (sda->compression.enabled) {
	    if (sda->state == SDA_STATE_DATAWRITE) {
		ZSTD_inBuffer input = { NULL, 0, 0 };
		size_t remaining = 0;
		do {
		    ZSTD_outBuffer output = {
			sda->compression.buf, sda->compression.buflen, 0 };
		    
		    remaining = ZSTD_compressStream2(sda->compression.zstd.cctx, &output , &input, ZSTD_e_end);
		    write(sda->fd, output.dst, output.pos);
		} while (remaining != 0);
		ZSTD_freeCCtx(sda->compression.zstd.cctx);
	    } else if (sda->state == SDA_STATE_DATAREAD) {
		ZSTD_freeDCtx(sda->compression.zstd.dctx);
	    }
	    free(sda->compression.buf);
	}
#endif
	close(sda->fd);
	if (sda->buf) free(sda->buf);
	free(sda);
    }
}

int
sda_get(sda_t sda, ...) {
    // Only if open read-only
    if (sda->mode != SDA_O_READONLY) {
	return -1;
    }

    if (sda->state != SDA_STATE_DATAREAD) {
	return -1;
    }

    ssize_t rc = 0;
    int count = 0;
    
    // Process data
    va_list args;
    va_start(args, sda);
    
    struct sda_field *field = sda->def->fields;
    for (; field->type ; field++, count++) {
	void  *ptr = va_arg(args, void *);
	
	switch(field->type) {
	case SDA_I8_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(int8_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_I16_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(int16_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_I32_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(int32_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_I64_TYPE:{
	    rc = sda_read(sda, ptr, sizeof(int64_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_U8_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(uint8_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_U16_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(uint16_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_U32_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(uint32_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_U64_TYPE:{
	    rc = sda_read(sda, ptr, sizeof(uint64_t));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_F32_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(float));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_F64_TYPE: {
	    rc = sda_read(sda, ptr, sizeof(double));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_CSTR_TYPE: {
	    rc = sda_read(sda, ptr, 0); // TODO
	    if (rc <= 0) goto failed;
	    assert(0);
	    break;
	}
	case SDA_BLOB_TYPE: {
	    assert(0);
	    break;
	}
	case SDA_CHAR_TYPE:  {
	    rc = sda_read(sda, ptr, sizeof(char));
	    if (rc <= 0) goto failed;
	    break;
	}
	case SDA_BOOL_TYPE:   {
	    char c;
	    rc = sda_read(sda, &c, sizeof(char));
	    if (rc <= 0) goto failed;
	    *(bool *)ptr = c == 'T' ? true : false;
	    break;
	}
	default:
	    assert(0);
	}
    }
    va_end(args);
    return count;
    
 failed:
    va_end(args);
    return rc;
}
    
    
int
sda_put(sda_t sda, ...) {
    // Not supported if open read-only
    if (sda->mode == SDA_O_READONLY) {
	return -1;
    }

    // Transition state if necessary
    switch(sda->state) {
    case SDA_STATE_META:
	if (sda->compress) {
	    sda_write_section_title(sda, "data:compressed");
	    sda_buffer_flush(sda);
	    sda_compress_init(sda);
	} else {
	    sda_write_section_title(sda, "data");
	}
	/* FALLTHROUGH */
    case SDA_STATE_DATAREAD:
	sda->state = SDA_STATE_DATAWRITE;
	break;
    case SDA_STATE_DATAWRITE:
	break;
    default:
	return -1;
    }

    // Process data
    va_list args;
    va_start(args, sda);

    struct sda_field *field = sda->def->fields;
    for (; field->type ; field++) {
	switch(field->type) {
	case SDA_I8_TYPE: {
	    int8_t d   = (int8_t)va_arg(args, int);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_I16_TYPE: {
#if INT16_MAX >= INT_MAX
	    int16_t d  = va_arg(args, int16_t);
#else
	    int16_t d  = (int16_t )va_arg(args, int);
#endif
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_I32_TYPE: {
#if INT32_MAX >= INT_MAX
	    int32_t d  = va_arg(args, int32_t);
#else
	    int32_t d  = (int32_t )va_arg(args, int);
#endif
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_I64_TYPE:{
	    int64_t d  = (int64_t )va_arg(args, int64_t);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_U8_TYPE: {
	    uint8_t d  = (uint8_t )va_arg(args, int);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_U16_TYPE: {
#if UINT16_MAX >= UINT_MAX
	    uint16_t d = va_arg(args, uint16_t);
#elif UINT16_MAX > INT_MAX
	    uint16_t d = (uint16_t)va_arg(args, unsigned int);
#else
	    uint16_t d = (uint16_t)va_arg(args, int);
#endif
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_U32_TYPE: {
#if UINT32_MAX >= UINT_MAX
	    uint32_t d = va_arg(args, uint32_t);
#elif UINT32_MAX > INT_MAX
	    uint32_t d = (uint32_t)va_arg(args, unsigned int);
#else
	    uint32_t d = (uint32_t)va_arg(args, int);
#endif
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_U64_TYPE:{
	    uint64_t d = (uint64_t)va_arg(args, uint64_t);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_F32_TYPE: {
	    float d    = (float   )va_arg(args, double);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_F64_TYPE: {
	    double d = va_arg(args, double);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_CSTR_TYPE: {
	    char *d = va_arg(args, char *);
	    sda_write(sda, d, strlen(d)+1);
	    break;
	}
	case SDA_BLOB_TYPE: {
	    assert(0);
	    break;
	}
	case SDA_CHAR_TYPE:  {
	    char d   = (char)va_arg(args, int);
	    sda_write(sda, &d, sizeof(d));
	    break;
	}
	case SDA_BOOL_TYPE:   {
	    bool d   = (bool)va_arg(args, int);
	    char c   = d ? 'T' : 'F';
	    sda_write(sda, &d, sizeof(c));
	    break;
	}
	default:
	    assert(0);
	}
    }
    va_end(args);

    return 0;
}
    
