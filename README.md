# Rational

CSV file is the de facto means of storing and exchanging small or huge
dataset, as they are very easy to generate and parse. Unfortunately
they are space hungry.

Several alternative exists such as
    [rds](https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/readRDS),
    [rdata](https://www.rdocumentation.org/packages/miceadds/versions/3.12-26/topics/load.Rdata),
    [hdf5](https://docs.hdfgroup.org/hdf5/develop/),
    [feather](https://arrow.apache.org/docs/python/feather.html), 
    [messagepack](https://msgpack.org/), 
    [bson](https://bsonspec.org/), ... 
but they can be complex to use or not necessary well suited for 
streaming large dataset.

This simple-data file format aim at:

* reducing disk usage (which will increase i/o throughput)
* add information about the dataset
* be straight forward in the code use to generate a dataset
  (nearly as easy as a `printf("%s,%d\n", a, b)` used to generate CSV)


# File format description

The file start with the magic string `# simple-data` and is composed of 2 parts:

* textual : hold the metadata, specification, description, ... (UTF-8 encoding)
* binary  : contains the data (can be zstd stream-compressed)

The textual part is prefixed by *#* character and contains:

* metadata      : information about the dataset, the author, the licensing, ...
* specification : how are coded the data and what they are (field description)
* description   : a free space for additional information (text, markdown, ...)

The following tags are allowed in the metadata, which if present immediately
follow the magic-string

| Tag         | Description                                 | Multiple |
|-------------|---------------------------------------------|----------|
| @title      | A short one-line title                      |          |
| @summary    | A multi-line summary                        |          |
| @license    | SPDX license identifier[^spdx]               |          |
| @author     | Author name / email                         |     ✔    |
| @url        | link to a description                       |     ✔    |
| @doi        | Digital Object Identifier System            |          |
| @keywords   | list of keywords (comma or space separated) |     ✔    |


Sections are started with the string `--<section-name>--` and follow the 
metada (if present), the _data_ section must be the last one.

| Section     | Description                                        | Required |
|-------------|----------------------------------------------------|----------|
| spec        | Definition of the fields (type, name, description) |    ✔     |
| description | Description of the dataset (text, markdown, ...)   |          |
| data        | The raw data (binary format)                       |    ✔     |

The specification (spec section) is a list of fields for which
type, name, unit (optional) and description (optional) are given.

~~~
type : name (description)
~~~

The available fields type are:

| Name | Description                  | Bytes |
|------|------------------------------|-------|
| i8   | integer                      |   1   |
| i16  | integer                      |   2   |
| i32  | integer                      |   4   |
| i64  | integer                      |   8   |
| u8   | unsigned integer             |   1   |
| u16  | unsigned integer             |   2   |
| u32  | unsigned integer             |   4   |
| u64  | unsigned integer             |   8   |
| f32  | float                        |   4   |
| f64  | double                       |   8   |
| cstr | c-string (nul-terminated)    |       |
| blob | binary data                  |       |
| char | single ASCII character       |   1   |
| bool | boolean                      |   1   |

* Data are stored in little endian format.
* Blob is a 2-byte integer holding the blob size, followed by the data


# File example

~~~
# simple-data:1
#
# @title     Simple-data example
# @summary   This dataset show usage of the various tags and sections
#            as well as the specifictions descriping the data.
# @license   CDLA-Sharing-1.0
# @author    Stéphane D'Alu <sdalu@sdalu.com>
# @keywords  example, dummy-set
#
# --<spec>--
# cstr : date   [date] (date in the yyyy-mm-dd format)
# cstr : event         (name of the event)
# i32  : count         (number of participants)
# f32  : age           (average age of participants)
#
# --<description>--
#
# Basic survery
#
# --<data>--
~~~


# Implementations

* c    : https://gitlab.inria.fr/dalu/simple-data/-/tree/main/c/
* ruby : https://gitlab.inria.fr/dalu/simple-data/-/tree/main/ruby/


[^spdx]: https://spdx.org/licenses/

