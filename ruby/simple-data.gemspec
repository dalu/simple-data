# -*- encoding: utf-8 -*-

require_relative 'lib/simple-data/version'

Gem::Specification.new do |s|
    s.name        = 'simple-data'
    s.version     = SimpleData::VERSION
    s.summary     = "Simple Data (CSV alternative)"
    s.description =  <<~EOF
      
      An alternative to CSV format for storing data.
      Provides metadata and field description support, and has a
      reasonable hunger for disk space.

      EOF

    s.homepage    = 'https://gitlab.inria.fr/dalu/simple-data'
    s.license     = 'MIT'

    s.authors     = [ "Stéphane D'Alu" ]
    s.email       = [ 'stephane.dalu@insa-lyon.fr' ]

    s.files       = %w[ README.md simple-data.gemspec ] +
                    Dir['lib/**/*.rb']

    s.add_dependency 'zstd-ruby'
    s.add_development_dependency 'yard', '~>0'
    s.add_development_dependency 'rake', '~>13'
end
