This is the ruby implementation of the [simple-data spec][sda]

# Example

~~~ruby
require 'simple-data'

SimpleData.generate('spacecraft.sda', 
                    [ [ :cstr, 'name',   nil, 'spacecraft'     ],
                      [ :cstr, 'origin', nil, 'serie or movie' ],
                      [ :f64,  'x',      'm'                   ],
                      [ :f64,  'y',      'm'                   ],
                      [ :f64,  'z',      'm'                   ] ],
                    tags: { author:  "Stephane D'Alu",
                            url:     "http://www.sdalu.com/",
                            license: "MIT" }) do |sda|
    sda.put("Enterprise", "Star Trek",    1.0, 2.0, 3.0);
    sda.put("Rocinante",  "The Expanse",  1e6, 2e6, 3e6);
    sda.put("Serenity",   "Firefly",      0.0, 0.0, 0.0);
    sda.put("Bebop",      "Cowboy Bebop", 4.0, 6.8, 8.0);
end

SimpleData.open('spacecraft.sda') do |sda|
    puts sda.fields.inspect
    while row = sda.get do
        puts row.inspect
    end
end
~~~

[sda]: https://gitlab.inria.fr/dalu/simple-data/

