class SimpleData
    def get_row
        if data = self.get
            Row.new(data, @fields_key)
        end
    end
end


class SimpleData
class Row
    def initialize(data, fields)
        @data_a = data
        @fields = fields
        @data_h = nil
    end

    def to_a
        @data_a
    end

    def to_h
        @data_h ||= [ @fields, @data_a ].transpose.to_h
    end

    def [](*args)
        if args.one?
            case arg = args[0]
            when Range, Integer then self.to_a[arg]
            when Symbol, String then self.to_h[arg.to_s]
            else raise ArgumentError
            end
        elsif args.all? {|v| v.is_a?(Integer) } && args.size == 2
            self.to_a[*args]
        elsif args.all? {|v| v.is_a?(Symbol) || v.is_a?(String) }
            args.map {|v| self.to_h[v.to_s] }
        else raise ArgumentError
        end
    end
end
end
