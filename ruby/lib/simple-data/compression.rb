
# Loading supported compressor
begin
    require 'zstd-ruby'
    require 'stringio'
rescue LoadError
    return
end


class SimpleData
    # Magic numbers
    MAGIC         = { "(\xB5/\xFD".force_encoding('BINARY') => :zstd
                    }

    # Compressed I/O wrapper
    class IOCompressedWrite
        def initialize(io, write: 16 * 1024)
            @io         = io
            @write_size = write
            @written    = 0
            @zstd       = Zstd::StreamingCompress.new
        end

        def close
            @io.write(@zstd.finish)
            @io.close
        end

        def write(data)
            @written += data.size
            if (@written > @write_size)
                @io.write(@zstd.flush)
                @io.flush
                @written = 0
            end
            @io.write(@zstd.compress(data))
        end
    end
    
    class IOCompressedRead
        def initialize(io, read: 16 * 1024)
            @io         = io
            @sio        = StringIO.new
            @read_size  = read

            # Peek at data for magic number
            pos = io.tell
            max = MAGIC.keys.map(&:size).max
            str = io.read(max)
            io.seek(pos)
        
            # Magic number lookup
            type = MAGIC.find {|k, v| str.start_with?(k) }&.last

            # Sanity check
            raise "data is not Zstd compressed" if type != :zstd

            # Decoder
            zstd = Zstd::StreamingDecompress.new
            @decoder = ->(str) { zstd.decompress(str) }
        end

        def eof?
            return false unless @sio.eof?
            return true  if     @io.eof?

            # Refill
            cstr = @io.read(@read_size)
            @sio.string = @decoder.call(cstr)

            # Recheck
            @io.eof? && @sio.eof?
        end
        
        def close
            @io.close
        end
        
        def read(size)
            data = @sio.read(size)

            # End of buffer
            if data.nil?
                if cstr = @io.read(@read_size)
                    # Refill buffer
                    @sio.string = @decoder.call(cstr)
                    read(size)
                else
                    # End of stream
                    nil
                end

            # Partial buffer
            elsif data.size < size
                # Force a new read (will trigger a refill)
                odata = read(size - data.size)
                odata.nil? ? data : (data + odata)

            # Full data
            else
                data
            end
        end

        def each_byte(&block)
            return to_enum(:each_byte) if block.nil?
            loop do
                @sio.each_byte(&block)
                break unless cstr = @io.read(@read_size)
                @sio.string = @decoder.call(cstr)
            end
        end
    end
    
end
