class SimpleData
    # Current file version
    FILE_VERSION  = 1

    # Various regex
    REGEX_MAGIC   = /\A# simple-data:(?<version>\d+)\s*\z/
    REGEX_SECTION = /\A# --<(?<section>[^>:]+)(?::(?<extra>[^>]+))?>--+\s*\z/
    REGEX_FIELD   = /\A\#\s*    (?<type>\w+      )       \s*:\s*
                                (?<name>[\w\-.:]+)
                            \s* (?:\[(?<unit>.*?     )\])?
                            \s* (?:\((?<desc>.*      )\))?   \s*\z
                                /ix
    REGEX_TAG     = /\A@(?<tag>\w+)\s+(?<value>.*?)\s*\z/
    REGEX_EMPTY   = /\A#\s*\z/

    # Supported tags / sections / types
    TAGS          = %i(title summary author license url doi keywords)
    SECTIONS      = %i(spec description data)
    TYPES         = %i(i8 i16 i32 i64 u8 u16 u32 u64 f32 f64
                       cstr blob char bool)

    # Error classes
    class Error < StandardError
    end
    class ParserError < Error
    end

    # Attributes
    attr_reader :version
    attr_reader :fields
    attr_reader :tags
    attr_reader :sections
    
    def initialize(io, fields, mode, version: FILE_VERSION,
                   tags: {}, sections: {})
        @io         = io
        @mode       = mode
        @fields     = fields
        @fields_key = fields.map {|(_, name)| name }
        @tags       = tags
        @sections   = sections
        @version    = version

        @read_ok, @write_ok =
                  case mode
                  when :create, :append then [ false, true  ]
                  when :read            then [ true,  false ]
                  else raise Error,
                             'mode must be one of (:create, :append, or :read)'
                  end
    end

    
    def put(*data)
        # Checking mode
        raise Error, "write is not allowed in #{@mode} mode" unless @write_ok


        if data.one? && (data.first.is_a?(Array) || data.first.is_a?(Hash))
            data = data.first
        end

        if data.size != @fields.size
            raise Error, 'dataset size doesn\'t match definition'
        end
                
        if data.is_a?(Hash)
            if ! (data.keys - @fields_key).empty?
                raise Error, 'dataset key mismatch'
            end

            data = @fields.map {|k| data[k] }
        end

        s = @fields.each.with_index.map {|(type,name), i| 
            d = data.fetch(i) { raise "missing data (#{name})" }
            case type
            when :i8   then [ d ].pack('c' )
            when :i16  then [ d ].pack('s<')
            when :i32  then [ d ].pack('l<')
            when :i64  then [ d ].pack('q<')
            when :u8   then [ d ].pack('C' )
            when :u16  then [ d ].pack('S<')
            when :u32  then [ d ].pack('L<')
            when :u64  then [ d ].pack('q<')
            when :f32  then [ d ].pack('e' )
            when :f64  then [ d ].pack('E' )
            when :cstr then [ d ].pack('Z*')
            when :blob then raise ParserError, 'not implemented'
            when :char then [ d ].pack('c' )
            when :bool then [ d ? 'T' : 'F' ]
            end
        }.join
        
        @io.write(s)
    end
    
    def get
        # Checking mode
        raise Error, "read is not allowed in #{@mode} mode" unless @read_ok

        # No-op if end of file
        return if @io.eof?

        # Retrieve data
        @fields.map {|(type)|
            case type
            when :i8   then @io.read(1).unpack1('c' )
            when :i16  then @io.read(2).unpack1('s<')
            when :i32  then @io.read(4).unpack1('l<')
            when :i64  then @io.read(8).unpack1('q<')
            when :u8   then @io.read(1).unpack1('C' )
            when :u16  then @io.read(2).unpack1('S<')
            when :u32  then @io.read(4).unpack1('L<')
            when :u64  then @io.read(8).unpack1('q<')
            when :f32  then @io.read(4).unpack1('e' )
            when :f64  then @io.read(8).unpack1('E' )
            when :cstr then @io.each_byte.lazy.take_while {|b| !b.zero? }
                                              .map {|b| b.chr }.to_a.join
            when :blob then @io.read(2).unpack1('s<')
                               .then {|size| @io.read(size) }
            when :char then @io.read(1)
            when :bool then @io.read(1) == 'T'
            end
        }
    end        

    def close
        @io.close
    end

    # Generating file
    def self.generate(file, fields,
                      compress: const_defined?(:IOCompressedWrite),
                      tags: nil, sections: nil, &block)
        # Sanity check
        if compress && !const_defined?(:IOCompressedWrite)
            raise Error, 'compression not supported (add zstd-ruby gem)'
        end

        # Open file
        io = File.open(file, 'w')

        # Magic string
        io.puts "# simple-data:1"

        # Tags
        if tags && !tags.empty?
            io.puts "#"
            tags.each do |name, value|
                io.puts "# @%-8s %s" % [ name, value ]
            end
        end

        # Spec
        io.puts "#"
        io.puts "# --<spec>--"
        name_maxlen = fields.map {|(_, name)| name.size }.max
        unit_maxlen = fields.map {|(_, _, unit)| unit&.size || 0 }.max
        
        fields.each do |(type, name, unit, desc)|
            if desc
                if unit
                    io.puts "# %-4s : %-*s [%-*s] (%s)" % [
                                type, name_maxlen, name, unit_maxlen, unit, desc ]
                else
                    io.puts "# %-4s : %-*s  %-*s  (%s)" % [
                                type, name_maxlen, name, unit_maxlen, '',   desc ]
                end
            else
                if unit
                    io.puts "# %-4s : %-*s [%-*s]" % [
                                type, name_maxlen, name, unit_maxlen, unit, ]
                else
                    io.puts "# %-4s : %s" % [ type, name ]
                end
            end
        end

        # Custom sections
        if sections && !sections.empty?
            sections.each do |name, value|
                io.puts "#"
                io.puts "# --<#{name}>--"
                value.split(/\r?\n/).each do |line|
                    io.puts "# #{line}"
                end
            end
        end

        # Data 
        io.puts "#"
        io.puts "# --<%s>--" % [ compress ? 'data:compressed' : 'data' ]

        # Deal with compression
        io = IOCompressedWrite.new(io) if compress
        
        # Instantiate SimpleData
        sda = self.new(io, fields, :create, tags: tags, sections: sections)
        block ? block.call(sda) : sda
    ensure
        sda.close if sda && block
    end
    
    # Open file for reading
    def self.open(file, mode = :read, &block)
        # Open file
        io = case mode
             when :read
                 File.open(file, 'r:BINARY')
             when :append
                 File.open(file, 'r+:BINARY').tap {|io|
                     io.seek(0, :END)
                 }
             else raise ArgumentError,
                        "mode must be one of :read, :append"
             end

        # Read textual information
        version                         = self.get_magic(io)
        fields, tags, sections, dataopt = self.get_metadata(io)

        # Deal with compression
        if dataopt&.include?(:compressed)
            unless const_defined?(:IOCompressedRead)
                raise Error, 'compression not supported (add zstd-ruby gem)'
            end
            io = IOCompressedRead.new(io)
        end

        # Instantiate SimpleData
        sda               = self.new(io, fields, mode, version: version,
                                     tags: tags, sections: sections)
        block ? block.call(sda) : sda
    ensure
        sda.close if sda && block
    end

    # Iterate over sda file
    def self.each(file)
        return to_enum(:each) unless block_given?

        self.open(file, :read) do |sda|
            while d = sda.get
                yield(d)
            end
        end
    end

        
    private
    
    def self.get_magic(io)
        unless m = REGEX_MAGIC.match(io.readline.chomp)
            raise ParserError, 'not a simple-data file'
        end
        m[:version]
    end

    def self.get_metadata(io)
        tags     = []
        fields   = []
        sections = {}
        dataopts  = nil

        # Retrieve meta data
        meta = io.each_line.lazy.map {|l| l.chomp }.take_while {|l|
            ! ((m = REGEX_SECTION.match(l)) &&
               (m[:section] == 'data'     )).tap {|is_data|
                dataopts = m[:extra]&.split(',')&.map(&:to_sym) if is_data
            }
        }.drop_while {|l| l =~ REGEX_EMPTY }

        # Parse
        meta.slice_before {|l| REGEX_SECTION =~ l }.each do |grp|
            if m = REGEX_SECTION.match(grp.first)
                grp.shift
                case s = m[:section].to_sym
                # Extract spec
                when :spec
                    fields = grp.reject {|l| l =~ REGEX_EMPTY }.map {|l|
                        field = REGEX_FIELD.match(l)&.captures
                        raise ParserError, "wrong spec" if field.nil?

                        # Normalize (type, name, unit, description)
                        t, n, u, d = field
                        t = t.downcase.to_sym
                        n .force_encoding('UTF-8')
                        d&.force_encoding('UTF-8')

                        # Sanity check
                        if !TYPES.include?(t)
                            raise ParserError, "unknown type (#{t})"
                        end

                        # Cleaned-up field description
                        [ t, n, u, d ]
                    }
                # Extract description
                else
                    grp = grp.map {|l| l.sub(/^#\s?/, '') }
                    grp = grp[...-1] if grp.last&.empty?
                    sections[s] = grp.join("\n")
                end
            else
                # Extract tags
                tags = grp.map          {|l| l.sub(/\A\#\s*/, '') }
                          .slice_before {|l| l =~  /\A@\w+/       }
                          .map          {|g| g.join(' ')          }
                          .map          {|tagline|
                    REGEX_TAG.match(tagline).captures
                }.reduce({}) { |obj, (t,v)|
                    obj.merge(t.to_sym => v) {|k, o, n| Array(o) + [ n ] }
                }

                # Normalize tags
                tags[:keywords] = Array(tags[:keywords]).flat_map {|e|
                    e.split(/\s*,\s*|\s+/).map(&:strip).uniq
                }
                
                # Tags sanityzing
                tags.each do |k, v|
                    if !TAGS.include?(k)
                        raise ParserError, "unknown tag (#{k})"
                    end
                end
                tags.reject! {|k, v| v.nil? || v.empty? }

            end
        end

        [ fields, tags, sections, dataopts ]
    end
    
end

require_relative 'simple-data/version'
require_relative 'simple-data/row'
require_relative 'simple-data/compression'
